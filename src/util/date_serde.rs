use chrono::NaiveDate;
use serde::{self, Serializer};

const FORMAT: &'static str = "%Y-%m-%d";

pub fn serialize<S>(date: &NaiveDate, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    let s = format!("{}", date.format(FORMAT));
    serializer.serialize_str(&s)
}
