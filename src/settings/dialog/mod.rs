mod app_page;
mod keybinding_editor;
mod shortcuts_page;
mod theme_chooser;
mod views_page;

use self::{app_page::AppPage, shortcuts_page::ShortcutsPage, views_page::ViewsPage};

use crate::app::Action;
use crate::settings::Settings;
use crate::util::BuilderHelper;
use glib::Sender;
use gtk::prelude::*;
use libhandy::PreferencesWindow;
use news_flash::NewsFlash;
use parking_lot::RwLock;
use std::sync::Arc;

pub struct SettingsDialog {
    pub widget: PreferencesWindow,

    _views_page: ViewsPage,
    _app_page: AppPage,
    _shortcuts_page: ShortcutsPage,
}

impl SettingsDialog {
    pub fn new(
        window: &libhandy::ApplicationWindow,
        sender: &Sender<Action>,
        settings: &Arc<RwLock<Settings>>,
        news_flash: &Arc<RwLock<Option<NewsFlash>>>,
    ) -> Self {
        let builder = BuilderHelper::new("settings");

        let dialog = builder.get::<PreferencesWindow>("dialog");
        dialog.set_transient_for(Some(window));

        let views_page = ViewsPage::new(&dialog, sender, settings);
        let app_page = AppPage::new(&dialog, sender, settings, news_flash);
        let shortcuts_page = ShortcutsPage::new(&dialog, sender, settings);

        dialog.add(&views_page.widget);
        dialog.add(&app_page.widget);
        dialog.add(&shortcuts_page.widget);

        let settings_dialog = SettingsDialog {
            widget: dialog,

            _views_page: views_page,
            _app_page: app_page,
            _shortcuts_page: shortcuts_page,
        };

        settings_dialog
    }
}
